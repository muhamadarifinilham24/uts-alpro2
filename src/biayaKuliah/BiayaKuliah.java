package biayaKuliah;
import java.util.Scanner;
public class BiayaKuliah {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        float hargaSKS = 75000;
        float denda = 5000;
        int jumSKS = 24;
        int tglAkhirBayaran = 5;
        float jumlahBayar = hargaSKS * jumSKS;
        System.out.println("jumlah yang harus dibayar = " + jumlahBayar);
        System.out.println("tanggal jatuh tempo = " + tglAkhirBayaran);
        System.out.print("masukan jumlah bayaran = ");
        float jumBayar = scanner.nextFloat();
        System.out.print("masukan tanggal bayar = ");
        int tglBayar = scanner.nextInt();

        float dendaBayar = (tglBayar > tglAkhirBayaran) ? denda : 0;
        float totalBayar = jumlahBayar + dendaBayar;
        float selisih = jumBayar - totalBayar;
        System.out.println("Denda yang harus dibayar = " + dendaBayar);
        System.out.println("Total yang harus dibayar = " + totalBayar);
        System.out.println("Selisih pembayaran = " + selisih);

        scanner.close();



    }
}
