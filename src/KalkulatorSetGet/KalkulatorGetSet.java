package KalkulatorSetGet;

import java.util.Scanner;

public class KalkulatorGetSet {
    private float nilai1;
    private float nilai2;

    public void setNilai1(float nilai1) {
        this.nilai1 = nilai1;
    }

    public void setNilai2(float nilai2) {
        this.nilai2 = nilai2;
    }

    public float getNilai1() {
        return nilai1;
    }

    public float getNilai2() {
        return nilai2;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        KalkulatorGetSet main = new KalkulatorGetSet();
        String pilihan;

        do {
            System.out.println("1. penjumlahan");
            System.out.println("2. pengurangan");
            System.out.println("3. perkalian");
            System.out.println("4. pembagian");
            System.out.println("5. keluar");
            System.out.print("\nMasukan pilihan Anda (1/2/3/4/5): ");
            pilihan = scanner.nextLine();

            switch (pilihan) {
                case "1":
                    System.out.println("Masukan nilai pertama: ");
                    main.setNilai1(scanner.nextFloat());
                    System.out.println("Masukan nilai kedua: ");
                    main.setNilai2(scanner.nextFloat());
                    float hasilTambah = main.getNilai1() + main.getNilai2();
                    System.out.println("Hasil dari penjumlahan nilai pertama dan kedua = " + hasilTambah);
                    break;

                case "2":
                    System.out.println("Masukan nilai pertama: ");
                    main.setNilai1(scanner.nextFloat());
                    System.out.println("Masukan nilai kedua: ");
                    main.setNilai2(scanner.nextFloat());
                    float hasilKurang = main.getNilai1() - main.getNilai2();
                    System.out.println("Hasil dari pengurangan nilai pertama dan kedua = " + hasilKurang);
                    break;

                case "3":
                    System.out.println("Masukan nilai pertama: ");
                    main.setNilai1(scanner.nextFloat());
                    System.out.println("Masukan nilai kedua: ");
                    main.setNilai2(scanner.nextFloat());
                    float hasilKali = main.getNilai1() * main.getNilai2();
                    System.out.println("Hasil dari perkalian nilai pertama dan kedua = " + hasilKali);
                    break;

                case "4":
                    System.out.println("Masukan nilai pertama: ");
                    main.setNilai1(scanner.nextFloat());
                    System.out.println("Masukan nilai kedua: ");
                    main.setNilai2(scanner.nextFloat());
                    float hasilBagi = main.getNilai1() / main.getNilai2();
                    System.out.println("Hasil dari pembagian nilai pertama dan kedua = " + hasilBagi);
                    break;

                case "5":
                    System.out.println("Terima kasih, sampai jumpa!");
                    break;

                default:
                    System.out.println("Pilihan tidak valid, silakan masukkan angka 1-5.");
            }

            scanner.nextLine();
        } while (!pilihan.equals("5"));

        scanner.close();
    }
}

