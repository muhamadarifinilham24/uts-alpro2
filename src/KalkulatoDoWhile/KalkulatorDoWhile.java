package KalkulatoDoWhile;
import java.util.Scanner;
public class KalkulatorDoWhile {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String pilihan;

        do {
            System.out.println("1. penjumlahan");
            System.out.println("2. pengurangan");
            System.out.println("3. perkalian");
            System.out.println("4. pembagian");
            System.out.println("5. keluar");
            System.out.print("\nMasukan pilihan Anda (1/2/3/4/5): ");
            pilihan = scanner.nextLine();

            switch (pilihan) {
                case "1":
                    System.out.println("Masukan nilai pertama: ");
                    float nilai1 = scanner.nextFloat();
                    System.out.println("Masukan nilai kedua: ");
                    float nilai2 = scanner.nextFloat();
                    float hasilTambah = nilai1 + nilai2;
                    System.out.println("Hasil dari penjumlahan nilai pertama dan kedua = " + hasilTambah);
                    break;

                case "2":
                    System.out.println("Masukan nilai pertama: ");
                    nilai1 = scanner.nextFloat();
                    System.out.println("Masukan nilai kedua: ");
                    nilai2 = scanner.nextFloat();
                    float hasilKurang = nilai1 - nilai2;
                    System.out.println("Hasil dari pengurangan nilai pertama dan kedua = " + hasilKurang);
                    break;

                case "3":
                    System.out.println("Masukan nilai pertama: ");
                    nilai1 = scanner.nextFloat();
                    System.out.println("Masukan nilai kedua: ");
                    nilai2 = scanner.nextFloat();
                    float hasilKali = nilai1 * nilai2;
                    System.out.println("Hasil dari perkalian nilai pertama dan kedua = " + hasilKali);
                    break;

                case "4":
                    System.out.println("Masukan nilai pertama: ");
                    nilai1 = scanner.nextFloat();
                    System.out.println("Masukan nilai kedua: ");
                    nilai2 = scanner.nextFloat();
                    float hasilBagi = nilai1 / nilai2;
                    System.out.println("Hasil dari pembagian nilai pertama dan kedua = " + hasilBagi);
                    break;

                case "5":
                    System.out.println("Terima kasih, sampai jumpa!");
                    break;

                default:
                    System.out.println("Pilihan tidak valid, silakan masukkan angka 1-5.");
            }
            scanner.nextLine();
        } while (!pilihan.equals("5"));
        scanner.close();

    }
}
